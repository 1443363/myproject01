package services.impl;

import model.Contact;

public class AuthorizationServiceImpl {

    public Contact authorize(String login, String password) {
        switch (login) {
            case "admin":
                if ("admin".equals(password)) {
                    return new Contact(1L, login, password);
                }
                break;
            case "user":
                if ("user".equals(password)) {
                    return new Contact(2L, login, password);
                }
                break;
            case "personal":
                if ("personal".equals(password)) {
                    return new Contact(3L, login, password);
                }
                break;
        }
        return null;
    }
}
