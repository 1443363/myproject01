package org.baeldung.servlet;

import model.Contact;
import services.impl.AuthorizationServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "loginServlet", urlPatterns = { "/auth" })
public class LoginServlet extends HttpServlet {

    private AuthorizationServiceImpl authorizationService = new AuthorizationServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        Contact contact = authorizationService.authorize(login, password);

        String contextPath = req.getContextPath();

        if (contact == null) {
            resp.sendRedirect(contextPath + "/login.jsp");
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("userId", contact.getId());
            resp.sendRedirect(contextPath + "/index.jsp");
        }
    }
}
