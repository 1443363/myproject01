package org.baeldung.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Hello world!
 *
 */
@WebServlet(name = "HelloAnnotationServlet", urlPatterns = {"/hello2"})
public class HelloAnnotationServlet extends HttpServlet {

    private String message;

    @Override
    public void init() throws ServletException {
        message = "Hello world (servlet via annotations)";
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html" );
        PrintWriter writer = resp.getWriter();
        writer.write("<p>" + message + "</p>");
    }

    @Override
    public void destroy() {
        //nothing
    }
}
